<?php

namespace Drupal\maintenance_mode_redirect\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\State\StateInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirects to a custom URL when maintenance mode is enabled.
 *
 * @package Drupal\maintenance_mode_redirect\EventSubscriber
 */
class MaintenanceModeRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The current request path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  // phpcs:disable Drupal.Files.LineLength.TooLong

  /**
   * Constructs a new \Drupal\maintenance_mode_redirect\EventSubscriber\MaintenanceModeRedirectSubscriber object.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current request path.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   */
  public function __construct(CurrentPathStack $current_path, ConfigFactoryInterface $config_factory, StateInterface $state, AccountProxy $current_user) {
    $this->currentPath = $current_path;
    $this->configFactory = $config_factory;
    $this->state = $state;
    $this->currentUser = $current_user;
  }

  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Set priority higher than core MaintenanceModeSubscriber events.
    $events[KernelEvents::REQUEST][] = ['redirectOnMaintenanceMode', 50];
    return $events;
  }

  /**
   * Redirects users when maintenance mode is enabled.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event object.
   */
  public function redirectOnMaintenanceMode(RequestEvent $event) {
    $config = $this->configFactory->get('maintenance_mode_redirect.settings');
    $has_permission = $this->currentUser->hasPermission('access site in maintenance mode');
    $maintenance_mode = $this->state->get('system.maintenance_mode');
    $redirect_url = $config->get('redirect_url');

    if (empty($redirect_url) || $maintenance_mode && $has_permission) {
      return;
    }

    $allowed_paths = [
      '/user',
      '/user/login',
      '/user/password',
    ];
    $configured_allowed_paths = $config->get('allowed_paths');
    $configured_allowed_paths_start_with = $config->get('allowed_paths_that_start_with');
    $current_path = $this->currentPath->getPath();
    $start_of_path_matches = FALSE;

    if (!empty($configured_allowed_paths)) {
      $configured_allowed_paths = explode("\r\n", $configured_allowed_paths);
      $allowed_paths = array_merge($configured_allowed_paths, $allowed_paths);
    }

    if (!empty($configured_allowed_paths_start_with)) {
      $configured_allowed_paths_start_with = explode("\r\n", $configured_allowed_paths_start_with);

      foreach ($configured_allowed_paths_start_with as $path) {
        if (strpos($current_path, $path) === 0) {
          $start_of_path_matches = TRUE;
          break;
        }
      }
    }

    if (!$start_of_path_matches && !in_array($current_path, $allowed_paths)) {
      // If system maintenance mode and maintenance redirect are enabled,
      // redirect users.
      $redirect_active = $config->get('redirect_active');

      if ($maintenance_mode && $redirect_active && !$has_permission) {
        $event->setResponse(new TrustedRedirectResponse($redirect_url));
      }
    }
  }

}
